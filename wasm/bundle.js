const { readFileSync, writeFileSync } = require('fs');

const wasmCode = readFileSync('wasm/jpqr.wasm');
const encoded = Buffer.from(wasmCode, 'binary').toString('base64');

writeFileSync('dist/wasm/jpqr.wasm.js', `export default '${encoded}';`);
