#!/bin/bash

cd `dirname $0`

# Go 1.13 and above
# cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" ../src/wasm/.
go get go.mercari.io/go-emv-code
GOOS=js GOARCH=wasm go build -o ../jpqr.wasm main.go
