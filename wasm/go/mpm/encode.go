package mpm

import (
	"log"
	"syscall/js"

	"go.mercari.io/go-emv-code/mpm"
	"go.mercari.io/go-emv-code/mpm/jpqr"
	"go.mercari.io/go-emv-code/tlv"
)

func Encode(v js.Value) js.Value {
	c := mpm.Code{
		PayloadFormatIndicator:     v.Get("payloadFormatIndicator").String(),
		PointOfInitiationMethod:    toPointOfInitiationMethod(v.Get("pointOfInitiationMethod")),
		MerchantAccountInformation: toMerchantAccountInformation(v.Get("merchantAccountInformation")),
		MerchantCategoryCode:       v.Get("merchantCategoryCode").String(),
		TransactionCurrency:        v.Get("transactionCurrency").String(),
		TransactionAmount:          toTransactionAmount(v.Get("transactionAmount")),
		CountryCode:                v.Get("countryCode").String(),
		MerchantName:               v.Get("merchantName").String(),
		MerchantCity:               v.Get("merchantCity").String(),
		PostalCode:                 v.Get("postalCode").String(),
		MerchantInformation:        toMerchantInformation(v.Get("merchantInformation")),
	}

	buf, err := jpqr.Encode(&c)
	if err != nil {
		log.Fatal(err)
	}
	return js.ValueOf(string(buf))
}

func toPointOfInitiationMethod(v js.Value) mpm.PointOfInitiationMethod {
	p := mpm.PointOfInitiationMethodStatic
	if v.String() == "dynamic" {
		p = mpm.PointOfInitiationMethodDynamic
	}
	return p
}

func toMerchantAccountInformation(v js.Value) []tlv.TLV {
	l := v.Length()
	m := make([]tlv.TLV, l)
	for i := 0; i < l; i++ {
		vi := v.Index(i)
		m[i] = tlv.TLV{
			Tag:    vi.Get("tag").String(),
			Length: vi.Get("length").String(),
			Value:  vi.Get("value").String(),
		}
	}
	return m
}

func toTransactionAmount(v js.Value) mpm.NullString {
	t := mpm.NullString{
		String: "",
		Valid:  false,
	}
	if !v.IsUndefined() && !v.IsNull() {
		t = mpm.NullString{
			String: v.String(),
			Valid:  true,
		}
	}
	return t
}

func toMerchantInformation(v js.Value) mpm.NullMerchantInformation {
	return mpm.NullMerchantInformation{
		LanguagePreference: v.Get("languagePreference").String(),
		Name:               v.Get("name").String(),
		Valid:              true,
	}
}
