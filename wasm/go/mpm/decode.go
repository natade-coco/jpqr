package mpm

import (
	"log"
	"syscall/js"

	"go.mercari.io/go-emv-code/mpm"
	"go.mercari.io/go-emv-code/mpm/jpqr"
	"go.mercari.io/go-emv-code/tlv"
)

func Decode(v js.Value) interface{} {
	p := []byte(v.String())

	c, err := jpqr.Decode(p)
	if err != nil {
		log.Fatal(err)
	}

	m := map[string]interface{}{
		"payloadFormatIndicator":     c.PayloadFormatIndicator,
		"pointOfInitiationMethod":    fromPointOfInitiationMethod(c.PointOfInitiationMethod),
		"merchantAccountInformation": fromMerchantAccountInformation(c.MerchantAccountInformation),
		"merchantCategoryCode":       c.MerchantCategoryCode,
		"transactionCurrency":        c.TransactionCurrency,
		"transactionAmount":          fromTransactionAmount(c.TransactionAmount),
		"countryCode":                c.CountryCode,
		"merchantName":               c.MerchantName,
		"merchantCity":               c.MerchantCity,
		"postalCode":                 c.PostalCode,
		"merchantInformation":        fromMerchantInformation(c.MerchantInformation),
	}

	return js.ValueOf(m)
}

func fromPointOfInitiationMethod(p mpm.PointOfInitiationMethod) js.Value {
	v := "static"
	if p == mpm.PointOfInitiationMethodDynamic {
		v = "dynamic"
	}
	return js.ValueOf(v)
}

func fromMerchantAccountInformation(ts []tlv.TLV) js.Value {
	v := make([]interface{}, len(ts))
	for i, t := range ts {
		v[i] = map[string]interface{}{
			"tag":    t.Tag,
			"length": t.Length,
			"value":  t.Value,
		}
	}
	return js.ValueOf(v)
}

func fromTransactionAmount(s mpm.NullString) js.Value {
	if !s.Valid {
		return js.ValueOf(nil)
	}
	return js.ValueOf(s.String)
}

func fromMerchantInformation(m mpm.NullMerchantInformation) js.Value {
	return js.ValueOf(map[string]interface{}{
		"languagePreference": m.LanguagePreference,
		"name":               m.Name,
	})
}
