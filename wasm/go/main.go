package main

import (
	"syscall/js"

	"./mpm"
)

type Object struct{}

func (t *Object) MPMEncode(this js.Value, args []js.Value) interface{} {
	return mpm.Encode(args[0])
}

func (t *Object) MPMDecode(this js.Value, args []js.Value) interface{} {
	return mpm.Decode(args[0])
}

func registerCallbacks() {
	var wasm = &Object{}
	js.Global().Set("wasm", js.ValueOf(
		map[string]interface{}{
			"mpmEncode": js.FuncOf(wasm.MPMEncode),
			"mpmDecode": js.FuncOf(wasm.MPMDecode),
		},
	))
}

func main() {
	c := make(chan struct{}, 0)
	registerCallbacks()
	<-c
}
