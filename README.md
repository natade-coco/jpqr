# JPQR

JPQR library that uses [go.mercari.io/go-emv-code](https://github.com/mercari/go-emv-code) by WebAssembly.

## Installation

```sh
npm install @natade-coco/jpqr
```

## Usage

```js
import JPQR from '@natade-coco/jpqr';

const code = JPQR.mpmEncode({
  payloadFormatIndicator: '01',
  pointOfInitiationMethod: 'dynamic',
  merchantAccountInformation: [{
    tag: '26',
    length: '68',
    value: '0019jp.or.paymentsjapan011300000000000010204000103060000010406000001'
  }],
  merchantCategoryCode: '5812',
  transactionCurrency: '392',
  transactionAmount: '498',
  countryCode: 'JP',
  merchantName: 'xxx',
  merchantCity: 'xxx',
  postalCode: '5300005',
  merchantInformation: {
    languagePreference: 'JA',
    name: 'ナタデココ カフェ'
  }
});

console.log(code);  // 00020101021226680019jp.or.paymentsjapan01130000000000001020400010306000001040600000152045812530339254034985802JP5903xxx6003xxx6107530000564190002JA0109ナタデココ カフェ63045BA0
showQRCode(code);
```

## API

### `mpmEncode(mpmInformation: MPMInformation): string`

Get JPQR MPM code for showing QR Code.
For details, please refer to JPQR MPM guideline 1.1.

#### MPMInformation

| param | type | example |
| :-- | :-- | :-- |
| payloadFormatIndicator | 01 | '01' |
| pointOfInitiationMethod | static: 静的, dynamic: 動的 | 'dynamic' |
| merchantAccountInformation | TLV[] | |
| merchantCategoryCode | string | '5812' |
| transactionCurrency | 392: 円 | '392' |
| transactionAmount? | ?string | '498' |
| countryCode | JP: 日本 | 'JP' |
| merchantName | string | 'xxx' |
| merchantCity | string | 'xxx' |
| postalCode | string | '5300005' |
| merchantInformation | MerchantInformation | |

##### TLV

| param | type | example |
| :-- | :-- | :-- |
| tag | string | '26' |
| length | string | '68' |
| value | string | '0019jp.or.paymentsjapan011300000000000010204000103060000010406000001' |

##### MerchantInformation

| param | type | example |
| :-- | :-- | :-- |
| languagePreference | JA: 日本語 | 'JA' |
| name | string | 'ナタデココ カフェ' |

### `mpmDecode(code: string): MPMInformation`

Get `MPMInformation` object from raw JPQR code.
