import base64 from './jpqr.wasm';

const convertToArray = text => {
  let bytes = new Uint8Array(text.length);
  for (let i = 0; i < text.length; i ++) {
    bytes[i] = text.charCodeAt(i);
  }
  return bytes;
};

let decoded = '';
if (typeof atob !== 'undefined') {
  decoded = atob(base64);
} else {
  decoded = Buffer.from(base64, 'base64').toString('ascii');
}
const bytes = convertToArray(decoded);

const go = new Go();
WebAssembly.instantiate(bytes.buffer, go.importObject)
  .then(({ instance }) => {
    go.run(instance);
  })
  .catch(error => {
    alert(error);
  });
