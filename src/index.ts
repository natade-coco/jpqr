import './wasm/wasm_exec';
import './wasm/wasm_load';

interface TLV {
  tag: string,
  length: string,
  value: string
}

interface MerchantInformation {
  languagePreference: 'JA',
  name: string
}

interface MPMInformation {
  payloadFormatIndicator: '01',
  pointOfInitiationMethod: 'static' | 'dynamic',
  merchantAccountInformation: TLV[],
  merchantCategoryCode: string,
  transactionCurrency:  string,
  transactionAmount?: string | null,
  countryCode: 'JP',
  merchantName: string,
  merchantCity: string,
  postalCode: string,
  merchantInformation: MerchantInformation
}

function mpmEncode(mpmInformation: MPMInformation): string {
  return wasm.mpmEncode(mpmInformation);
};

function mpmDecode(code: string): MPMInformation {
  return wasm.mpmDecode(code);
};

export default {
  mpmEncode,
  mpmDecode
};
